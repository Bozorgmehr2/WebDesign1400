class car(object):
    """docstring"""
    def __init__(self, color, doors, tires):
        """Constructor"""
        self.color = color
        self.doors = doors
        self.tires = tires

    def brake(self):
        """
        Stop the car
        """
        return "Braking"

    def drive(self):
        """
        Drive the car
        """
        return "I’m driving!"

if __name__== "__main__" : 
    c = car("red",2,4)
    print(c.brake())
    print(c.drive())
    print(f"Doors : {c.doors} - Color : {c.color} - Tires : {c.tires}")



