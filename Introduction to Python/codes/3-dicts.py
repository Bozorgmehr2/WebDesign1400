import json

employee1 = {
    "name" : "ali",
    "family" : "ahmadi",
    "age" : 19,
    "educated" : True
}

print(employee1)
print(employee1["name"])
print(f"{employee1['name']} {employee1['family']}")
employee1["city"] = "yazd"
print(employee1)
# del employee1["city"]
# print(employee1)

em1 = json.dumps(employee1,indent=2)
print(em1)

print("^"*25)
print(employee1.items())
print("^"*25)

for k,v in employee1.items() :
    print(f"{k} : {v}")

f = open(r"E:\Web Design\Introduction to Python\codes\employees.txt","w")
f.write(json.dumps(employee1,indent=2))
f.close()

f = open(r"E:\Web Design\Introduction to Python\codes\employees.txt","r")
employee1 = json.load(f)
print(json.dumps(employee1,indent=4))

employee1["university"] = []
bs = {
    "name" : "birjand",
    "major" : "cs",
    "avg" : 16.45
}
ms = {
    "name" : "tabriz",
    "major" : "cs",
    "avg" : 17.25
}

employee1["university"].append(bs)
employee1["university"].append(ms)
print(json.dumps(employee1,indent=2))


employee1["university"] = {}
bs = {
   
    "name" : "birjand",
    "major" : "cs",
    "avg" : 16.45
    
}
ms = {
   
    "name" : "tabriz",
    "major" : "cs",
    "avg" : 17.25
    
}

employee1["university"]["bs"] = bs
employee1["university"]["ms"] = ms


print(json.dumps(employee1, indent=2))
print(employee1["university"]["bs"]["avg"])

f = open(r"E:\Web Design\Introduction to Python\codes\employees.txt","w")
f.write(json.dumps(employee1,indent=2))
f.close()

